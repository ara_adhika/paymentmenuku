<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Order;
use App\User;
use App\Pemberitahuan;

/*
development

merchant id: 33069
user id: bot33069
passwrd: p@ssw0rd
merchant name: e-warnet

prod
merchant id: 33069
user id: bot33069
passwrd: 7jql8iEW

*/


class FaspayController extends Controller {

    /* 
    merchant id: 33595
    user id: bot33595
    password: p@ssw0rd
    merchant name: Menuku

    ----------------------------------
    Docs  --> https://faspay.co.id/docs/index-business.html#integration
    Account testing --> https://faspay.co.id/docs/index-testing.html#account-testing
    Simulator Payments <--> https://dev.faspay.co.id/simulator
    */
 
    private $logName ;
    /* DEVEL */
    // const MERCHANT_NAME = "Menuku";
    // const USER_ID       = "bot33595";
    // const PASSWD        = "p@ssw0rd";
    // const MERCHANT_ID   = "33595";

    /* PROD */
    const MERCHANT_NAME = "Menuku";
    const USER_ID       = "bot33595";
    const PASSWD        = "i9g2wgyq";
    const MERCHANT_ID   = "33595";

    public function __construct()
    {
        //$this->middleware('auth');
        $this->logName = "menuku-faspay-" . date("Ymd") . ".log";
    }

    public function paymentnotifva (Request $request) {
        /*
        url development buat terima payment notifiksi dari faspay.
        kalo mau dikirim ke redpay:
        1. json yg diterima dari faspay, dicek apakah payment_status_code=2
        2. kalo != 2, do nothing.
        3. kalo == 2, payment sukses, kirim ke url faspay_vadynamic di web redpay.
        4. json yg diterima, echo/return ke faspay.

        */
            /*{
            "request": "Payment Notification",
            "trx_id": "3183540500001172",
            "merchant_id": "31835",
            "merchant": "Panin Asset Management",
            "bill_no": "220171004154635022158001",
            "payment_reff": "null",
            "payment_date": "2017-10-04 15:46:35",
            "payment_status_code": "2",
            "payment_status_desc": "Payment Sukses",
            "bill_total": "5000000",
            "payment_total": "5000000",
            "payment_channel_uid": "402",
            "payment_channel": "Permata Virtual Account",
            "signature": "075c4983ba9883d41e1b3eab0de580dfc73d875b"
            }

        Response:
        {
        "response": "Payment Notification",
        "trx_id": "3183540500001172",
        "merchant_id": "31835",
        "merchant": "Panin Asset Management",
        "bill_no": "220171004154635022158001",
        "response_code": "00",
        "response_desc": "Sukses",
        "response_date": "2017-10-05 16:53:10"
        }
        */

        $isSukses = false;
        $resp = $request->getContent();
        Storage::append($this->logName, "[".date("H:i:s")."] RECV " . $request->ip() . " " . $resp);

        $data = json_decode($resp);
        if (!$data) {
            return response()->json(['status' => 'failed',
                                    'message' => 'no json data received']);
        }

        switch ($data->payment_status_code) {
            case "0"  : # Belum diproses
                        $result = ["response" => "Payment Notification",
                                "trx_id"   => $data->trx_id,
                                "merchant_id" => self::MERCHANT_ID,
                                "merchant" => self::MERCHANT_NAME,
                                "bill_no" => $data->bill_no,
                                "response_code" => "10",
                                "response_desc" => "Gagal, belum diproses",
                                "response_date" => date("Y-m-d H:i:s")
                                ];
                        break;
                        
            case "1"  : # Dalam proses
                        $result = ["response" => "Payment Notification",
                                "trx_id"   => $data->trx_id,
                                "merchant_id" => self::MERCHANT_ID,
                                "merchant" => self::MERCHANT_NAME,
                                "bill_no" => $data->bill_no,
                                "response_code" => "30",
                                "response_desc" => "Gagal, dalam diproses",
                                "response_date" => date("Y-m-d H:i:s")
                                ];
                        break;

            case "2"  : # Payment Sukses
                        # bill_no : 6a5922ab33844357b01b7d00597fb975
                        # orderid : 6a5922ab-3384-4357-b01b-7d00597fb975

                        $orderid = $this->idWithDashed($data->bill_no);
                        Storage::append($this->logName, "[".date("H:i:s")."] Convert bill_no " . $data->bill_no . " to orderid " .$orderid);
                        $order = Order::find($orderid);
                    
                        if ($order != null ) {
                            #dd($faspay);
                            # update status tabel orders, kirim json response.
                            # cek, order_status == 1, 
                            # data->payment_total == order->total_payment,
                            # data->trx_id == order->payment_code 

                            $_data = json_decode($resp, true);
                            $_data['user_id'] = self::USER_ID;
                            $_data['password'] = self::PASSWD;
                            $_data['verifyUrl'] = 'https://web.faspay.co.id/cvr/100004/10';        

                            $verifyResp = $this->_verifyPayment($_data); # cek payment inquiry
                            

                            if ($verifyResp['payment_status_code'] == "2") {
                                    $isSukses = true;
                                    $order->order_status = 2;
                                    $order->remark .= "|" . $resp;
                                    $order->save();
        
                                    #kirim SMS
                                    $urlStatus = "https://apps.menuku.co.id/currorderstatus/". $order->id;
                                    //dibikin bit.ly dulu kalo bisa.

                                    $tgl = Carbon::now()->toDateTimeString(); #2021-06-03 HH:mm:ss
                                    $tgl = Carbon::now()->settings(['locale' => 'id_ID'])->isoFormat('D MMMM YYYY HH:mm');

                                    $pesan = "Terima kasih sudah bertransaksi di MENUKU. Pesanan no " . $order->invoice_number . 
                                            " sudah kami terima pembayarannya pada " . $tgl .
                                            " sebesar Rp. " . number_format($order->total_payment, 0, ",", ".") . 
                                            " melalui channel " . $data->payment_channel . "." ;

                                    $shortLink = $this->shortlink($urlStatus);
                                    if ($shortLink != "nolink") {
                                        $pesan .= " Status order " . $shortLink ; 
                                    }
                                        
                                    $this->sendSMS($order->customer_phone, $pesan);
        
                                    #kirim Notifications
                                    //insert data ke tabel notification untuk setiap user role kitchen untuk idstore ini.
                                    $kitchenUser = User::where('employe_role', 'Kitchen')
                                                    ->where('id_store', $order->id_store)
                                                    ->get();
                                                    
                                    foreach ($kitchenUser as $user) {
                                        $dataNotif['title'] = 'Pesanan Baru';
                                        $dataNotif['message'] = 'Pesanan tamu ' . $order->customer_name;
                                        $dataNotif['username'] = $user->email;
                                        $dataNotif['idorder'] = $order->id;
                                        Pemberitahuan::create($dataNotif);
                                    }

                                    $result = ["response" => "Payment Notification",
                                                "trx_id"   => $data->trx_id,
                                                "merchant_id" => self::MERCHANT_ID,
                                                "merchant" => self::MERCHANT_NAME,
                                                "bill_no" => $data->bill_no,
                                                "response_code" => "00",
                                                "response_desc" => "Sukses",
                                                "response_date" => date("Y-m-d H:i:s")
                                            ];
        
                            } else {
                                $result = ["response" => "Payment Notification",
                                            "trx_id"   => $data->trx_id,
                                            "merchant_id" => self::MERCHANT_ID,
                                            "merchant" => self::MERCHANT_NAME,
                                            "bill_no" => $data->bill_no,
                                            "response_code" => "21",
                                            "response_desc" => "Gagal",
                                            "response_date" => date("Y-m-d H:i:s")
                                                ];

                            }

                        } else {

                            #gagal topup   
                            $result = ["response" => "Payment Notification",
                                    "trx_id"   => $data->trx_id,
                                    "merchant_id" => self::MERCHANT_ID,
                                    "merchant" => self::MERCHANT_NAME,
                                    "bill_no" => $data->bill_no,
                                    "response_code" => "25",
                                    "response_desc" => "Gagal, data transaksi tidak ada",
                                    "response_date" => date("Y-m-d H:i:s")
                                    ];

                        }
                        break;

            case "3"  : # Payment gagal
                        //break;

            case "4"  : # Payment Reversal
                        //break;

            case "5"  : # Tagihan tidak ditemukan
                        //break;

            // case "6"  : # Payment 
            //             break;

            case "7"  : # Payment expired
                        //break;

            case "8"  : # Payment Cancelled
                        //break;
                            
            case "9"  : # Unknown
                        $result = ["response" => "Payment Notification",
                                "trx_id"   => $data->trx_id,
                                "merchant_id" => self::MERCHANT_ID,
                                "merchant" => self::MERCHANT_NAME,
                                "bill_no" => $data->bill_no,
                                "response_code" => "50",
                                "response_desc" => "Gagal, belum diproses (".$data->payment_status_code.")",
                                "response_date" => date("Y-m-d H:i:s")
                                ];
                        
                        break;
        }

        Storage::append($this->logName, "[".date("H:i:s")."] RESP paymentnotif " . json_encode($result) );          
        return response()->json($result);


    }

    public function chargeRequest(Request $request, $payment_method) {

        $billno = $request->input('billno');
        $jumlahtransfer = $request->input('jumlahtransfer');
        $nama = $request->input('nama');
        $msisdn = $request->input('msisdn');
        $email = $request->input('email');
        
        Storage::append($this->logName, "[".date("H:i:s")."] " . "RECV " . $request->ip() . json_encode($request->all()));
        

        $params = [ "request"      => "Info Detil Pembelian",
                    "merchant_id"  => self::MERCHANT_ID,
                    "merchant"     => self::MERCHANT_NAME,
                    "bill_no"      => $billno, #32 character0
                    "bill_reff"     => $billno, #32 char (opsional)
                    "bill_date"    => date("Y-m-d H:i:s"), 
                    "bill_expired" => Carbon::now()->addhours(1)->toDateTimeString(),
                    "bill_desc"    => "Pembayaran #" . $billno, #128 alfanumerik
                    "bill_currency" => "IDR",
                    "bill_gross"   => "0", #opsional bill wo tax
                    "bill_tax"     => "0", #opsional 
                    "bill_miscfee" => "0", #opsional misc fee
                    "bill_total"   => (string)($jumlahtransfer*100) , 
                    "payment_channel" => $payment_method,
                    "pay_type"     => "1",
                    "cust_no"      => "10",
                    "cust_name"    => $nama,
                    "bank_user_id" => "",
                    "msisdn"       => $msisdn,
                    "email"        => $email,
                    "terminal"     => "10",
                    "billing_address" =>"",
                    "billing_address_city" => "",
                    "billing_address_region" => "",
                    "billing_address_state" => "",
                    "billing_address_poscode" => "",
                    "billing_address_country_code" => "",
                    "receiver_name_for_shipping" => "",
                    "shipping_address" => "",
                    "shipping_address_city" => "",
                    "shipping_address_region" => "",
                    "shipping_address_state" => "",
                    "shipping_address_poscode" => "",
                    "shipping_address_country_code" => "",
                    "item" => [ "0" => array("product" => "Topup saldo",
                                "amount"  => (string)(($jumlahtransfer)*100) ,
                                "qty"    => "1",
                                "payment_plan" => "1",
                                "tenor"  => "00",
                                "merchant_id" => self::MERCHANT_ID)
                            ],
                    "reserve1" => "",
                    "reserve2" => "",
                    "signature" => sha1(md5(self::USER_ID . self::PASSWD . $billno))
                ];

        /*development*/
        //$urlFaspay = "https://dev.faspay.co.id/cvr/300011/10";

        /* production */
        $urlFaspay = "https://web.faspay.co.id/cvr/300011/10";

        $ch = curl_init($urlFaspay);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json")); 
        curl_setopt($ch, CURLINFO_HEADER_OUT , true);
        // curl_setopt($ch, CURLOPT_SSL_VERIFYSTATUS, true);

                                                
        Storage::append($this->logName, "[".date("H:i:s")."] " . "SEND $urlFaspay " . json_encode($params));
        //Storage::append($this->logName, "[".date("H:i:s")."] " . json_encode($params));
        
        $resp = curl_exec($ch); 
        Storage::append($this->logName, "[".date("H:i:s")."] " . "RECV faspay $resp");

        if( curl_errno($ch) != 0 ) 
        { 
            $resp = 'something happend, curl_code: ' . curl_errno($ch);
            $status = "Gagal Curl errno:". curl_errno($ch);
            Storage::append($this->logName,  "[".date("H:i:s")."] GAGAL resp " . curl_errno($ch) . ", " . curl_error($ch));
            
        } else {
            $data = json_decode($resp);
            $status = "got response from faspay";
            $info = curl_getinfo($ch);

            Storage::append($this->logName,  "[".date("H:i:s")."] " . "Header : " . json_encode($info));
            //$this->saveResp($data, $depo->id, \Auth::user()->name);
            
            // if ($data->response_code == "00") {
            //      return redirect($data->redirect_url);            
            // }
            
        }
        curl_close($ch);    

        //return redirect("http://www.warnetpay.id");
        return response()->json(json_decode($resp));

    }


    private function _verifyPayment($data ) {
            $reqParams = ["request" => "Pengecekan Status Pembayaran",
                        "trx_id"  => $data['trx_id'],
                        "merchant_id" => self::MERCHANT_ID,
                        "bill_no" => $data['bill_no'],
                        "signature" => sha1(md5($data['user_id'] . $data['password'] . $data['bill_no'])), #sha1(md5((user_id + password + bill_no))
                        ];

                        //dd(json_encode($reqParams));

                        $curl = curl_init();
                        curl_setopt_array($curl, array(
                        CURLOPT_URL => $data['verifyUrl'],
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_POST => true,
                        CURLOPT_POSTFIELDS => json_encode($reqParams),
                        CURLOPT_HTTPHEADER => array(
                        "Content-Type: application/json"
                        ),
                        ));

                        Storage::append($this->logName, "[".date("H:i:s")."] SEND verifyPayment " . $data['verifyUrl'] . " " .  json_encode($reqParams));
                        $response = curl_exec($curl);
                        Storage::append($this->logName, "[".date("H:i:s")."] RECV verifyPayment " . $response );
                        $responseArray = json_decode($response,true);

            return $responseArray;            
    }


    public function callback (Request $request) {
        //https://dev2.faspay.co.id/merchantpage_v2/billing/process/thankyou?
        //trx_id=8985999990013136&
        //merchant_id=99999&
        //merchant=FASPAY+STORE&
        //bill_no=4370&
        //bill_ref=4370&
        //bill_total=10000&
        //bank_user_name=FaspayTest&
        //status=0&
        //signature=0da33ad07fe9980f7b7ff70a964803e821a86866

        return view('faspay.callback')->with('qs', $request->all());
    }


    private function sendSMS($msisdn, $pesan) {
      $uid="redision";
      #$mask="Redision";
      $mask="SUWUN";
      $ts = time();
      $appkey= "FurSuseD8CDNWNBsc1cbUJFJFMskSUby";
      $secret = "m13qei8q6c5a5vw";
      $signature=md5($uid . $mask . $msisdn . $pesan . $ts . $appkey . $secret);

      $data = "signature=$signature&uid=$uid&mask=$mask&d=$msisdn&m=" . urlencode($pesan) . "&ts=$ts" ;   

      $url = "http://202.53.250.117:8182/SMSGW/sendm";
      $ch = curl_init($url. "?$data");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_POST, false);
      curl_setopt($ch, CURLOPT_TIMEOUT, 60);
      $resp = curl_exec($ch); 
      if( curl_errno($ch) != 0 ) { 
          $resp = 'something happend, curl_code: ' . curl_errno($ch);
          $status = "Gagal Curl errno:". curl_errno($ch);
      } else {
          $status = "SUKSES kirim SMS";
      }
      curl_close($ch);    
    }   


    private function idWithDashed($bill_no) {
        # bill_no : 6a5922ab33844357b01b7d00597fb975
        # orderid : 6a5922ab-3384-4357-b01b-7d00597fb975
        $part[0] = substr($bill_no, 0, 8);
        $part[1] = substr($bill_no, 8, 4);
        $part[2] = substr($bill_no, 12, 4);
        $part[3] = substr($bill_no, 16, 4);
        $part[4] = substr($bill_no, 20);
        return implode("-", $part);
    } 

    /**
     * Get the value of data
     */ 
    public function getData() {
                        return $this->data;
    }


    private function shortlink($long_url) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://api-ssl.bitly.com/v4/shorten',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>'{
        "long_url": "'.$long_url.'",
        "domain": "bit.ly"
        }',
        CURLOPT_HTTPHEADER => array(
            'Authorization: Bearer a1ea780047bb7aa4c4f968a7186e07fa0bce3acc',
            'Content-Type: application/json'
        ),
        ));

        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        Storage::append($this->logName , "[". date("H:i:s") ."] SEND bitly " . $long_url);
        $response = curl_exec($curl);
        Storage::append($this->logName , "[". date("H:i:s") ."] RECV bitly httpcode:" . $httpcode . " " .  $response);

        curl_close($curl);
        $json = json_decode($response);

        if ($httpcode== "200" || $httpcode == "201" || $httpcode == "0") {
            return $json->link;

        } else {
            return "nolink";
        }

    }

}
